<?php
namespace app;

use app\Promo;
use app\DB;

class Application{
	
	public $title = 'Web application';
	public $note = array();
	public $errors = array();
	
	
	public function __construct(){
		$page = 'pageUpload';
		
		if(isset($_FILES['file']))
			$page = 'pageResult';
		
		$this->$page();
	}	
	
	
	private function checkTable(){
		$dbConnect = new DB();
		$table = $dbConnect->db->query('SHOW TABLES LIKE "promo"');
		
		if($table->rowCount() == 0){
			$dbConnect->db->query('
				CREATE TABLE IF NOT EXISTS `promo` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`title` varchar(255) NOT NULL,
					`date_start` int(11) NOT NULL,
					`date_end` int(11) NOT NULL,
					`status` int(1) NOT NULL,
					`url` varchar(255) NOT NULL,
					PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1
			');
		}
		else
			$this->note[] = 'Таблица promo уже существует. Если данные из импортируемого файла уже есть в БД, они будут заменены.';
	}
	
	
	
	private function pageUpload(){
		$this->title = 'Импорт CSV файла';
		require_once('views/pages/upload.php');
	}
	
	
	private function pageResult(){
		$this->title = 'Обработка файла';
		
		if(!isset($_FILES['file']) || ($_FILES['file']['type'] != 'application/vnd.ms-excel')){			
			$this->errors[] = 'Файл не загружен или у файла неверный формат.';
			$this->pageUpload();
		}
		else{
			$this->checkTable();
			move_uploaded_file($_FILES['file']['tmp_name'], 'assets/files/file.csv');
			
			$i = 0;
			$loadedIdList = array(); // список id данных из файла
			$urlList = array(); // здесь будем хранить сгенерированные URL адреса
			
			if (($handle = fopen("assets/files/file.csv", "r")) !== FALSE) {
				while (($row = fgetcsv($handle, 0, ";")) !== FALSE) {
					
					$i++;
					if($i == 1) continue;
					
					$promo = new Promo();
					
					$promo->findById($row[0]);
					
					$row[1] = iconv('windows-1251', 'UTF-8', $row[1]);
					
					$promo->setData(array(
						'id' => $row[0],
						'title' => $row[1],
						'date_start' => strtotime($row[2]),
						'date_end' => strtotime($row[3]),
						'status' => (($row[4] == 'On') ? 1 : 0)
					));
					
					
					if($promo->save()){
						$loadedIdList[] = $row[0];
						$urlList[] = $promo->url;
					}
					else
						$this->errors[] = 'Ошибка в строке '.$i.':<ul><li>'. join('</li><li>', $promo->errors). '</li></ul>';
				}
				fclose($handle);
			}
			
			@unlink("assets/files/file.csv");
			
			$randomItemIndex = rand(0, count($loadedIdList)-1);
			$randomItem = new Promo();
			$randomItem->findById($loadedIdList[$randomItemIndex]);
			$randomItem->status = $randomItem->status*-1 + 1;
			$randomItem->save();
			
			require_once('views/pages/result.php');
		}
	}
}

?>