<?php
namespace app;

use PDO;

class DB{
	
	
	private $dbName = 'choco';
	private $dbHost = 'localhost';
	private $dbUser = 'root';
	private $dbPassword = '';
	
	public $db = false;
	
	public function __construct(){
		$dsn = 'mysql:host='.$this->dbHost.';dbname='.$this->dbName.';charset=utf8';
		$opt = array(
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, // выдавать исключение при ошибке
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC // данные всегда в виде именованного массива
		);
		
		$this->db = new PDO($dsn, $this->dbUser, $this->dbPassword, $opt);
	}
	
}

?>