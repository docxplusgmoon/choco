<?php
namespace app;

use app\DB;

class Promo{
	public $id;
	public $title;
	public $date_start;
	public $date_end;
	public $status;
	public $url;
	public $errors = array();
	
	private $isNewRecord = true;
	private $fields = array('id', 'title', 'date_start', 'date_end', 'status', 'url');
	
	
	// метод находит запись по id
	public function findById($id){
		$dbConnect = new DB();		
		$dbQuery = $dbConnect->db->prepare('SELECT * FROM promo WHERE id = ?');
		$dbQuery->execute(array($id));
		
		if($dbQuery->rowCount() == 1){
			$this->setData($dbQuery->fetch());
			$this->isNewRecord = false;
		}
		
	}
	
	
	// метод добавляет новую или обновляет запись в базе данных. 
	public function save(){
		
		$query = array();
		$values = array();
		
		if(!$this->validate()) return false;
		
		$this->generateUrl();
		
		foreach($this->fields as $field){
			$query[] = '`'.$field.'` = ?';
			$values[] = $this->$field;
		}
		
		if($this->isNewRecord)
			$sql = 'INSERT INTO promo SET '.join(', ', $query);
		else
			$sql = 'UPDATE promo SET '.join(', ', $query).' WHERE id = "'.$this->id.'"';
		
		$dbConnect = new DB();		
		$dbQuery = $dbConnect->db->prepare($sql);
		$dbQuery->execute($values);
		return true;
	}
	
	
	public function setData($data){
		foreach($data as $attr=>$value)
			$this->$attr = $value;
	}
	
	private function validate(){
		if(!is_numeric($this->id) || $this->id <= 0)
			$this->errors[] = 'Неверно указан id записи';
		
		if(trim($this->title) == '')
			$this->errors[] = 'Неверно указана название акции';
		
		if(!is_numeric($this->date_start) || $this->date_start <= 0)
			$this->errors[] = 'Неверно указана дата старта акции';
		
		if(!is_numeric($this->date_start) || $this->date_start <= 0)
			$this->errors[] = 'Неверно указана дата окончания акции';
		
		return (count($this->errors) > 0) ? false : true;
	}
	
	
	private function generateUrl(){
		$arTranslit = array("а" => "a", "ый" => "iy", "ые" => "ie", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "yo", "ж" => "zh", "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "kh", "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "shch", "ь" => "", "ы" => "y", "ъ" => "", "э" => "e", "ю" => "yu", "я" => "ya", "йо" => "yo", "ї" => "yi", "і" => "i", "є" => "ye", "ґ" => "g");
		
		
		$title = mb_strtolower($this->title, 'UTF-8');
		$title = strtr($title, $arTranslit);
		$title = preg_replace("/[^0-9a-zA-Zа-яА-Я]+/i", "-", $title);
		
		$this->url = $this->id . '-' . trim($title, '-');
	}
}
?>