<?php require('views/header.php'); ?>

<?php 
	if(count($this->note) > 0)
		echo '
			<div class="note">
				<ul>
					<li>'. join('</li><li>', $this->note) .'</li>
				</ul>
			</div>
		';
	
	if(count($this->errors) > 0)
		echo '
			<div class="err">
				<ul>
					<li>'. join('</li><li>', $this->errors) .'</li>
				</ul>
			</div>
		';
?>


<div class="random_item">
	<a href="/" class="back">Назад</a>
	<h3>Случайная запись</h3>
	
	<table>
		<thead>
			<tr>
				<th width="30%">Атрибут</th>
				<th width="70%">Значение</th>				
			</tr>
		</thead>
		<tbody>
			<tr>
				<td width="30%">
					ID акции
				</td>
				<td width="70%">
					<?=$randomItem->id;?>
				</td>
			</tr>
			
			<tr>
				<td width="30%">
					Название акции
				</td>
				<td width="70%">
					<?=htmlspecialchars($randomItem->title);?>
				</td>
			</tr>
			
			<tr>
				<td width="30%">
					Дата начала акции
				</td>
				<td width="70%">
					<?=date('d-m-Y', $randomItem->date_start);?>
				</td>
			</tr>
			
			<tr>
				<td width="30%">
					Дата окончания
				</td>
				<td width="70%">
					<?=date('d-m-Y', $randomItem->date_end);?>
				</td>
			</tr>
			
			<tr>
				<td width="30%">
					Статус
				</td>
				<td width="70%">
					<?=($randomItem->status == 0) ? 'On' : 'Off';?>
				</td>
			</tr>
			
			<tr>
				<td width="30%">
					URL адрес
				</td>
				<td width="70%">
					<?=$randomItem->url;?>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="url_list">
	<h3>Список сгенерированных URL адресов</h3>
	<p>
	<?=join('</p><p>', $urlList);?>
	</p>
</div>






<?php require('views/footer.php'); ?>