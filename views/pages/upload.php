<?php require('views/header.php'); ?>

<?php if(count($this->errors) > 0) : ?>
	<div class="err"><?=join('<br>', $this->errors);?></div>
<?php endif; ?>

<div class="form">
	<form action="/" method="post" enctype="multipart/form-data">		
		<input type="file" name="file" required class="form_file">
		<input type="submit" name="upload" >
	</form>
</div>

<?php require('views/footer.php'); ?>